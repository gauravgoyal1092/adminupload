# 3rd party package
from django_q.tasks import async_task

# Django import 
from django.contrib import admin

# app models import
from app.models import Document, Result


class DocumantAdmin(admin.ModelAdmin):

	def save_model(self, request, obj, form, change):
		is_created = obj.pk == None and change == False
		super().save_model(request, obj, form, change)
		if is_created:
			async_task('app.task.process_uploaded_file', obj)

		return obj
	
admin.site.register(Document, DocumantAdmin)
admin.site.register(Result)
