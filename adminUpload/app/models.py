# Python imports
import csv
from io import BytesIO

# 3rd party packages import
import pandas as pd
import xlrd

# Django import
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.contrib.postgres.fields import JSONField

# app imports
from adminUpload import settings

def file_size(value):
	"""
	Validator to check file size
	"""
	limit = settings.FILE_SIZE_LIMIT
	if value.size > limit:
		raise ValidationError('File too large. Size should not exceed 20 MiB.')

class Document(models.Model):
    title = models.CharField(max_length=255, blank=False)
    document = models.FileField(
    	upload_to='documents/',
    	validators=[
    		FileExtensionValidator(allowed_extensions=settings.ALLOWED_FILE_EXTENSIONS),
    		file_size
    	],
    	help_text="""
    		Uploaded document should be less than 20 MB in size and 
    		file extensions should be .csv, .xls or .xlsx.
    	"""
    )

    def __str__(self):
    	return self.title

    def clean(self):
    	"""
    	Cleaner for Document model.
    	Check uploaded files and see if header contains Geography & Count
    	"""
    	document_name = self.document.name
    	headers = []

    	if "xls" in document_name or "xlsx" in document_name:
    		book = xlrd.open_workbook(file_contents=self.document.read())
    		df = pd.read_excel(book, engine='xlrd', header=None)
    	elif "csv" in document_name:
    		data = BytesIO(self.document.read())
    		df = pd.read_csv(data, sep=",", header=None)
    	
    	data = df.values.tolist()
    	headers = [str(h).lower() for h in data[0]]

    	if "geography" not in headers or "count" not in headers:
    		raise ValidationError(
    			"Invalid File passed. We were not able to find correct headers"
    		)


class Result(models.Model):
	geography = models.CharField(max_length=255, blank=False)
	count = models.CharField(max_length=255, blank=False)
	extra_params = JSONField(default=dict)

	def __str__(self):
		return self.geography