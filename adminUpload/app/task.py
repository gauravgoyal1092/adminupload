import pandas as pd

# model imports
from app.models import Result

def process_uploaded_file(document):
	"""
	Run this Task after saving new document via admin panel.

	Read files using pandas according to file extension.
	After reading data convert to list rather than using numpy array.

	Get header index for geography & count and create Result objects.
	"""
	filename = document.document.name
	if ".csv" in (document.document.name):
		df = pd.read_csv(
			document.document, sep=",", header=None
		)
	else:
		df = pd.read_excel(
			document.document, engine='xlrd', header=None
		)

	data = df.values.tolist()
	header = [str(h).lower() for h in data[0]]

	geography_header_index = header.index("geography")
	count_header_index = header.index("count")

	results = []
	for line in data[1:]:
		extra_params = {}
		geography = line[geography_header_index]
		count = line[count_header_index]
		for index, value in enumerate(line):
			if index not in [geography_header_index, count_header_index]:
				extra_params[header[index]] = value

		results.append(
			Result(
				geography=geography,
				count=count,
				extra_params=extra_params
			)
		)
	Result.objects.bulk_create(results)