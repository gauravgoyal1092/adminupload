Django==3.0.3
django-picklefield==2.1.1
django-q==1.1.0
pandas==1.0.0
psycopg2-binary==2.8.4
redis==3.4.1
xlrd==1.2.0
