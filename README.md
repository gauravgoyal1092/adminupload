Upload Document from admin to create Model data.

Development Setup
-----------------

* Create and activate virtualenv
* Clone Project and run `pip install -r requiremennts.txt`
* Create postgres database with name - `adminUpload`.
* Run migrations - `python manage.py migrate`
* Run project by command - `python manage.py runserver`
* Run djnagoq cluster - `python manage.py qcluster`
* Run redis server
* Create superuser by running command - `python manage.py createsuperuser`


How To Create Data
------------------

* Open admin panel after running all servers - `localhost:8000/admin/`
* Login using superuser
* Go into Documents and create object.
* Document field in Documents model has file size validation of 20 MB which can
be changed in settings.py in adminUpload app, file type validation of .csv,
.xls and .xlsx, Header validations which states file to invalid if geography and
count not in file headers

* Successful creation of document object start a task to create Result objects.
* All the errors and completed tasks are created in Django Q Failed & Successful
tasks models

